cmake_minimum_required(VERSION 3.13)
project(Mem_checking C)

set(CMAKE_C_STANDARD 99)

add_executable(Mem_checking main.c memchk.c mem.h assert.c assert.h except.c except.h)