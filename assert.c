#include "assert.h"

#ifdef NO_DEBUG

#else
const Except_T Assert_Failed = { "Assertion failed" };

void (assert)(int e) {
    assert(e);
}
#endif

/*
 * The parentheses around the name assert in the function definition
 * suppress expansion of the macro assert and thus define the function,
 * as required in the interface.
 */