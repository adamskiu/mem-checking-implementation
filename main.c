#include <stdio.h>
#include "mem.h"

int main() {
    int *tab = ALLOC(30 * sizeof(int));
    FREE(tab);
    return 0;
}