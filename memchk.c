#include <stdlib.h>
#include <string.h>
#include "assert.h"
#include "except.h"
#include "mem.h"

const Except_T Mem_Failed = { "Allocation Failed" };
/*
 * Checking types
 * checking macros
 * data
 * checking data
 * checking functions
 */

/* Checking types */

/* This alignment ensures that any type of data can be stored
 * in the blocks returned by Mem_alloc. If the ptr passed to Mem_free isn't so
 * aligned, it can't possibly be in htab and is thus invalid. */
union align {
    int i;
    long l;
    long *lp;
    void *p;
    void (*fp)(void);
    float f;
    double d;
    long double ld;
};

/*
 * Mem_free and Mem_resize can detect access errors if Mem_alloc,
 * Mem_calloc, and Mem_resize never return the same address twice
 * and if they remember all of the addresses they do return
 * and which ones refer to allocated memory.
 * Abstractly, these functions maintain a set S whose elements
 * are the pairs (ptr, free) or (ptr, allocated), where ptr is the
 * address returned by an allocation.
 * The value free indicates that the address ptr does not refer to
 * allocated memory; that is, it has been deallocated explicitly,
 * and the value allocated indicates that ptr points to allocated memory.
 *
 *
 * Mem_alloc and Mem_calloc add the pair (ptr, allocated) to S, where ptr
 * is their return value, and they guarantee that neither (ptr, allocated)
 * nor (ptr, free) was in S before the addition.
 *
 * Mem_free(ptr) is legal if ptr is null or if (ptr, allocated) is in S.
 * If ptr is nonnull and (ptr, allocated) is in S, Mem_free deallocates
 * the block at ptr and changes the entry in S to (ptr, free).
 *
 * Mem_resize(ptr, nbytes,...) is legal only if (ptr, allocated) is in S.
 * If so, Mem_resize calls Mem_alloc to allocate a new block, copies the
 * contents of the old one to the new one, and calls Mem_free, and calls
 * Mem_free to deallocate the old one; these calls make the appropriate
 * changes to S.
 *
 * The condition that the allocation functions never return the same address
 * twice can be implemented by never deallocating the byte at an address
 * previously returned by an allocation function.
 *
 * S can be implemented by keeping a table of the addresses of these bytes.
 *
 * This scheme can be implemented by writing a memory allocator that sits
 * on top of the standard library functions. This allocator maintains a hash
 * table of block descriptors.
 *
 * struct descriptor {
    struct descriptor *free;
    struct descriptor *link;
    const void *ptr;
    long size;
    const char *file;
    int line;
};

 ptr is the address pf the block, which is allocated elsewhere
 size is the size of the block.

 file and line are the block's allocation coordinates - the source
 coordinates passed to the function that allocated the block.
 These vales aren't used, but they're store in descriptors so that debuggers
 can print them during a debugging session.
 */

/* Checking macros */

/* The hash macro treats the address as a bit pattern,
 * shifts it right three bits, and reduces it modulo the size of htab. */
#define hash(p, t) (((unsigned long)(p)>>3) & \
    (sizeof (t) / sizeof ((t)[0])-1))

#define NDESCRIPTORS 512
#define NALLOC ((4096 + sizeof (union align) -1)/ \
    (sizeof (union align)))*(sizeof (union align))

/* Checking data */
static struct descriptor {
    struct descriptor *free;
    struct descriptor *link;
    const void *ptr;
    long size;
    const char *file;
    int line;
} *htab[2048];

/* Dummy descriptor */
static struct descriptor freelist = { &freelist };

/* Checking functions */

static struct descriptor *find(const void *ptr) {
    struct descriptor *bp = htab[hash(ptr, htab)];

    while (bp && bp->ptr != ptr)
        bp = bp->link;
    return bp;
}

/* If ptr is nonnull and is a valid address,
 * the block is deallocated by appending it to the
 * free list for possible reuse by a subsequent call to Mem_alloc */
void Mem_free(void *ptr, const char *file, int line) {
    if (ptr) {
        struct descriptor *bp;
        /* set bp if ptr is valid
         * if ptr isn't an allocated pointer
         * and it wasn't freed before
         * ((unsigned long)ptr)%(sizeof (union align)) != 0 avoids calls to find
         * for those addresses that aren't multiples of the strictest alignment
         * and thus cannot possibly be valid block pointers
         * */
        if (((unsigned long)ptr)%(sizeof (union align)) != 0
        || (bp = find(ptr)) == NULL || bp->free)
            Except_raise(&Assert_Failed, file, line);

        bp->free = freelist.free;
        freelist.free = bp;
    }
}

void *Mem_resize(void *ptr, long nbytes, const char *file, int line) {
    struct descriptor *bp;
    void *newptr;

    assert(ptr);
    assert(nbytes > 0);
    if (((unsigned long)ptr)%(sizeof (union align)) != 0
        || (bp = find(ptr)) == NULL || bp->free)
        Except_raise(&Assert_Failed, file, line);

    newptr = Mem_alloc(nbytes, file, line);
    memcpy(newptr, ptr,
           nbytes < bp->size ? nbytes : bp->size);
    Mem_free(ptr, file, line);
    return newptr;
}

void *Mem_calloc(long count, long nbytes, const char *file, int line) {
    void *ptr;

    assert(count > 0);
    assert(nbytes > 0);
    ptr = Mem_alloc(count * nbytes, file, line);
    memset(ptr, '\0', count * nbytes);
    return ptr;
}

/* dalloc allocates, initializes, and returns one descriptor,
 * dolling it out of the 512 descriptor chunks obtained from malloc.*/
static struct descriptor *dalloc(void *ptr, long size, const char *file, int line) {
    static struct descriptor *avail;
    static int nleft;

    if (nleft <= 0) {
        /* allocate descriptors */
        avail = malloc(NDESCRIPTORS * sizeof(*avail));
        if (avail == NULL)
            return NULL;
        nleft = NDESCRIPTORS;
    }
    avail->ptr = ptr;
    avail->size = size;
    avail->file = file;
    avail->line = line;
    avail->free = avail->link = NULL;
    nleft--;
    return avail++;
}

/* For loop starts at the beginning of the free list.
 * The first free block whose size exceeds nbytes is used to fill
 * the request. The nbytes at the end of this free block are carved off,
 * and the address of that block is returned after its descriptor
 * is created, initialized and added to htab.
 * The test bp->size > nbytes guarantees that the value of bp->ptr
 * is never reused. */
void *Mem_alloc(long nbytes, const char *file, int line) {
    struct descriptor *bp;
    void *ptr;

    assert(nbytes > 0);
    //round nbytes up to alignment boundary
    nbytes = ((nbytes + sizeof(union align) - 1) /
             (sizeof(union align))) * (sizeof(union align));
    for (bp = freelist.free; bp; bp = bp->free) {
        if (bp->size > nbytes) {
            // use the end of the block at bp->ptr
            bp->size -= nbytes;
            ptr = (char *)bp->ptr + bp->size;
            if ((bp = dalloc(ptr, nbytes, file, line)) != NULL) {
                unsigned h = hash(ptr, htab);
                bp->link = htab[h];
                htab[h] = bp;
                return ptr;
            } else {
                // Raise Mem_Failed
                if (file == NULL)
                    RAISE(Mem_Failed);
                else
                    Except_raise(&Mem_Failed, file, line);
            }
        }
        if (bp == &freelist) {
            struct descriptor *newptr;
            // newptr <- a block of size NALLOC + nbytes
            if ((ptr = malloc(nbytes + NALLOC)) == NULL
            ||  (newptr = dalloc(ptr, nbytes + NALLOC, __FILE__, __LINE__)) == NULL) {
                if (file == NULL)
                    RAISE(Mem_Failed);
                else
                    Except_raise(&Mem_Failed, file, line);
            }
            newptr->free = freelist.free;
            freelist.free = newptr;
        }
    }
    assert(!"Can't happen bug!");
    return NULL;
}