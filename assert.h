#ifndef ASSERT_INCLUDED
#define ASSERT_INCLUDED

#undef assert
#ifdef NO_DEBUG
#define assert(e) ((void)0)
#else
#include "except.h"
extern Except_Frame *Except_stack;
extern const Except_T Assert_Failed;
extern void assert(int e);
#define assert(e) ( (void) ((e) || (RAISE(Assert_Failed), 0)) )
#endif

/*
#include <stdlib.h>

#undef assert
#ifdef NO_DEBUG
#define assert(e) ((void)0)
#else
extern void assert(int e);

#define assert(e) ((void)((e)|| \
    (fprintf(stderr, "%s:%d: Assertion failed: %s\n", __FILE__, (int)__LINE__, #e), \
    abort(), 0)))

#endif
*/



/*
 * In assert macro entire expression is cast to void because
 * the standard stipulates that assert(e returns no value.
 * In standard C #e generates a string literal whose content
 * are the characters in the text for the expression e.
 */


#endif // ASSERT_INCLUDED