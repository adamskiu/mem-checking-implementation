#ifndef MEM_INCLUDED
#define MEM_INCLUDED
#include "except.h"

extern const Except_T Mem_Failed;

extern void *Mem_alloc (long nbytes,
     const char *file, int line);
extern void *Mem_calloc(long count, long nbytes,
     const char *file, int line);
extern void  Mem_free  (void *ptr,
     const char *file, int line);
extern void *Mem_resize(void *ptr, long nbytes,
    const char *file, int line);

#define ALLOC(nbytes) \
    Mem_alloc((nbytes), __FILE__, __LINE__)

#define CALLOC(count, bytes) \
    Mem_calloc((count), (nbytes), __FILE__, __LINE__)

#define  NEW(p) ((p) = ALLOC((long)sizeof(*(p))))

#define NEWO(p) ((p) = CALLOC(1, (long)sizeof(*(p))))

#define FREE(ptr) ((void)(Mem_free((ptr), \
    __FILE__, __LINE__), (ptr) = 0))

#define RESIZE(ptr, nbytes) ((ptr) = Mem_resize((ptr), \
    (nbytes), __FILE__, __LINE__))

#endif

/*
 * Mem_alloc allocates a block of at least nbytes
 * and returns a pointer to the first byte.
 * The block is aligned on an addressing boundary
 * that is suitable for the data with the strictest
 * alignment requirement.
 *
 * Mem_calloc allocates a block large enough to
 * hold an array of count elements each of size bytes,
 * and returns a pointer to the first element.
 * The block is aligned as for Mem_alloc, and is initialized to zeros.
 * The null pointer and 0.0 are not necessarily represented
 * by zeros, so Mem_calloc may not initialize them correctly.
 *
 * Mem_free takes a pointer to the block to be deallocated.
 * If ptr is non-null Mem_free deallocates that block;
 * if ptr is null, Mem_free has no effect.
 *
 * Mem_resize changes the size of the block allocated by a previous call to Mem_alloc,
 * Mem_calloc, or Mem_resize. Mem_resize expands or contracts the block so that it holds
 * at least nbytes of memory, suitably aligned, and returns a pointer
 * to the resized block. Mem_resize may move the block in order to change its size,
 * so Mem_resize is logically equivalent to allocating a new block, copying some
 * or all of the data from ptr to the new block, and deallocating ptr.
 * If nbytes exceeds the size of the block pointed to by ptr, the excess bytes
 * are uinitialized. Otherwise, nbytes beginning at ptr are copied to the new block.
 *
 *
 * NEW(p) allocates an uninitialized block to hold *p and sets p
 * to the address of that block.
 *
 * NEWO(p) does the same, but also clears the block.
 *
 * FREE(ptr) takes a pointer to a block, calls Mem_free to
 * deallocate the block, and sets prt to the null pointer.
 *
 * RESIZE(ptr, nbytes) changes ptr to point at the new block - a common use
 * of Mem_resize.
 */

/*
 * Checked runtime errors:
 * - To nbytes in Mem_alloc, Mem_calloc of Mem_resize to be nonpositive
 * - To pass a null ptr to Mem_resize
 *
 * ONLY IN CHECKING IMPLEMENTATION:
 * - To pass Mem_free or Mem_resize a non-null ptr that was not returned by a
 *   previous call to Mem_alloc, Mem_calloc, or Mem_resize, or a ptr
 *   that has already been passed to Mem_free or Mem_resize.
 *
 * Unchecked runtime errors:
 * ONLY IN PRODUCTION IMPLEMENTATION:
 *  - To pass Mem_free or Mem_resize a non-null ptr that was not returned by a
 *    previous call to Mem_alloc, Mem_calloc, or Mem_resize, or a ptr
 *    that has already been passed to Mem_free or Mem_resize.
 *
 * Exceptions:
 * - Mem_alloc, Mem_calloc and Mem_resize raise Mem_Failed if
 *   they cannot allocate the memory requested and
 *   pass file and line to Except_raise so the exceptions
 *   report the location of the call.
 */